package com.user.repository;

import java.util.List;

import com.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	User findUserByfname(String fname);
	List<User> findUserBypincode(String pincode);

	User findUserById(int id);
}
